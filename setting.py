TRADE_SIZE_PERCENT = 0.03
LEVERAGE = 25
INTERVAL = 10
URL = 'https://testnet.bitmex.com'
API_ID = 'TiqIfiGD_7F3hr5RQtZ70kbX'
API_SEC = 'fd2OkzIlb5wPdsJtiqGmOdGBRAtUWfiY3U4FQV1ujAGlERah'
SCOPES = ['https://mail.google.com', 
          'https://www.googleapis.com/auth/gmail.modify']

alert_bin = {
    'strict_exit':
    ['Exit Short', 'Short Stop Loss', 'Exit Long', 'Long Stop Loss'],
    'smart_exit': ['Long', 'Short'],
    'exit_side': {
        'Exit Short': 'Buy',
        'Short Stop Loss': 'Buy',
        'Exit Long': 'Sell',
        'Long Stop Loss': 'Sell'
    },
    'entry_side': {
        'Long': 'Buy',
        'Short': 'Sell'
    }
}
