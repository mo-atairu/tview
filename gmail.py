from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from setting import SCOPES


STORE = file.Storage('credentials.json')
CREDS = STORE.get()

if not CREDS or CREDS.invalid:
    flow = client.flow_from_clientsecrets('oauth.json', SCOPES)
    CREDS = tools.run_flow(flow, STORE)

gmail = build('gmail', 'v1', http=CREDS.authorize(Http()))


def get_message_id():
    '''
    Notice: This module may fail silently
    '''
    try:
        email = gmail.users().messages()
        msg_id = email.list(userId='me', labelIds='Label_7204165564382795302',
                          maxResults=1, q='is:unread')
        return msg_id.execute()['messages'][0]['id']
    except KeyError as err:
        return None
    except Exception as err:
        return None


def get_message(msg_id):
    '''
    Notice: This module may fail silently
    '''
    try:
        email = gmail.users().messages()
        msg = email.get(userId='me', id=msg_id, format='full').execute()
        headers = msg['payload']['headers']
        return list(filter(lambda s: s['name'] == 'Subject', headers))[0]        
    except Exception as err:
        return None


def get_alert():
    msg_id = get_message_id()
    if msg_id:
        msg = get_message(msg_id=msg_id)
        return msg['value'].split(':')[1].strip()


def mark_msg_as_read():
    msg_id = get_message_id()
    email = gmail.users().messages()
    unread = email.modify(userId='me', id=msg_id,
                          body={'removeLabelIds': ["UNREAD"]})
    unread.execute()
